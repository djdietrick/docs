# React

React is a Javascript/Typescript framework for creating responsive single page applications in the web.

## Creating an App

```bash
npm create vite@latest <app-name> -- --template=react-ts
cd <app-name>
npm install
npm run dev
```

### Adding Tailwindcss (Optional)

```bash
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
```

```js
// tailwind.config.js
/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [],
};
```

```css
/* index.css */
@tailwind base;
@tailwind components;
@tailwind utilities;
```
