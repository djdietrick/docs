export default {
  title: "Docs.Dietrick.Dev",
  descript: "A collection of notes and snippets",
  base: "/",
  themeConfig: {
    socialLinks: [
      { icon: "github", link: "https://gitlab.com/djdietrick/docs" },
    ],
    sidebar: {
      "/python/": require("../python/sidebar.json"),
      "/rust/": require("../rust/sidebar.json"),
      "/nuxt/": require("../nuxt/sidebar.json"),
      "/ts/": require("../ts/sidebar.json"),
      "/terraform/": require("../terraform/sidebar.json"),
      "/interview/": require("../interview/sidebar.json"),
      "/server/": require("../server/sidebar.json"),
      "/aws/": require("../aws/sidebar.json"),
      "/go": require("../go/sidebar.json"),
      "/react": require("../react/sidebar.json"),
      "/java": require("../java/sidebar.json"),
      "/": [
        {
          text: "Home",
          items: [
            { text: "Introduction", link: "/" },
            { text: "Languages", link: "/languages" },
            { text: "Frameworks", link: "/frameworks" },
            { text: "Devops", link: "/devops" },
            { text: "Interview Prep", link: "/interview/" },
            { text: "Server", link: "/server/" },
            { text: "AWS", link: "/aws/" },
          ],
        },
      ],
    },
  },
};
