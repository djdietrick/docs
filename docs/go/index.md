# Go

Go is a statically-typed, compiled, and open source programming language created by Google.

## Creating a New Project

```bash
mkdir <project_name>
cd <project_name>
go mod init gitlab.com/djdietrick/<project_name>
```
