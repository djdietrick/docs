# Pointers

Pointers, like C++, hold memory addresses of a value. The zero value of a pointer is `nil`. Pointers are declared with `*T` which is a pointer to type T. The `&` operator creates a pointer to its operand. You can access the pointer value with `*` to get and set.

```go
var p *int
i := 42
p = &i
fmt.Println(*p)
*p = 21
```

## Structs

To access values of a struct you are referencing with a pointer, you can use the shorthand of the `.` operator to access the value.

```go
type Vertex struct {
    X int
    Y int
}
p := &Vertex{1,2}
fmt.Println(p.Y)
p.X = 4
```
