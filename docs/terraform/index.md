# Terraform

Terraform is a software that allows you to define your infrastructure as code. You can automate your deployment, keep track of the state of your environments, and have an audit of all versions that have been deployed to an environment.

## Installation

Installation can be done from the [terraform website](http://www.terraform.io), where you can download the binary and add it to your path. If on mac, this can be done with brew.
