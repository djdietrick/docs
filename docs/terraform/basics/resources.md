# Resources

Resources should be defined in `instance.tf`. Before creating a resource you must first create a provider in `provider.tf`.  A provider is an entity such as AWS, GCP, DigitalOcean, etc.  Once you specify a provider, run `terraform init` to download that provider's module. Within these providers are services which we can use via resources.

```hcl
# provider.tf
provider "aws" {
    # version = xxx
}

# vars.tf
variable "AWS_REGION" {
    type = "string"
}

variable "AMIS" {
    type = map
    default = {
        us-central-1: "my ami"
    }
}

# instance.tf
resource "aws_instance" "example1" {
    ami = ${lookup(var.AMIS, var.AWS_REGION)}
    instance_type = "t2.micro"
}

```