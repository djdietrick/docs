# NAS Setup

## Creating Mountable Volume

After creating a new shared folder in the NAS configuration, you then need to enable NFS for that folder.  Click `Edit` and go to `NFS Permissions`.  Add a new rule for client `*` with read/write permissions, and check "Enable asynchronous" and "Access to subfolders".

## Mounting to Linux

```bash
sudo apt update
sudo apt install nfs-common

sudo mount -t nfs <nas_ip>:/volumeX/<shared_folder> <mount_location>

ex:
sudo mount -t nfs 192.168.0.103:/volume1/media /media/nas
```

## Auto mount on startup

Edit `/etc/fstab` file and add a line like below:

```bash
<nas_ip>:/volumeX/<shared_folder> <mount_location> nfs nfsvers=3 0 0
```

Then run `sudo mount -a` to mount the drive.