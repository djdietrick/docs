# Dates

```python
import datetime

t = datetime.time(4, 20, 1)

# Let's show the different components
print(t) # 04:20:01
print('hour  :', t.hour) # 4
print('minute:', t.minute) # 20
print('second:', t.second) # 1
print('microsecond:', t.microsecond) # 0
print('tzinfo:', t.tzinfo) # None

today = datetime.date.today()
print(today) # YYYY-MM-DD
print('ctime:', today.ctime()) 
print('tuple:', today.timetuple())
print('ordinal:', today.toordinal())
print('Year :', today.year)
print('Month:', today.month)
print('Day  :', today.day)

d1 = datetime.date(2015, 3, 11)
print('d1:', d1) # 2015-03-11

d2 = d1.replace(year=1990)
print('d2:', d2) # 1990-03-11

result = d1 - d2
print(result.days) # 9131
print(result.seconds) # 0
print(result.total_seconds) # difference in seconds
```