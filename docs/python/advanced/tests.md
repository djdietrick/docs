# Testing

## Basic file format
```python
import unittest

class MyTest(unittest.TestCase):

    def test_one(self):
        self.assertEqual(1,1)

if __name__ == '__main__':
    unittest.main()
```

## Assertions

| Assertion      | Logic    |
| --------- | ------------- |
| assertEqual(a, b) | a == b |
| assertNotEqual(a, b) | a != b |
| assertTrue(x) | bool(x) is True |
| assertFalse(x) | bool(x) is False |
| assertIs(a, b) | a is b |
| assertIsNot(a, b) | a is not b |
| assertIsNone(x) | x is None |
| assertIsNotNone(x) | x is not None |
| assertIn(a, b) | a in b |
| assertNotIn(a, b) | a not in b |
| assertIsInstance(a, b) | isinstance(a, b) |
| assertNotIsInstance(a, b) | not isinstance(a, b) |