# Debugging

Debugging is achieved by using the `pdb` package and setting a breakpoint with `pdb.set_trace()`.  From that point, you get a python shell with access to all of the objects currently in scope.

To navigate through the code, you can use `n` to go to the next line, or `s` to step in to a function call.  When ready to continue the processing, use `c`.

```python
import pdb

x = [1,2,3]
y = 2
z = 3

res1 = y + z
pdb.set_trace()
res2 = x + y
```