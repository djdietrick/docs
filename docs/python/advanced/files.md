# Files

## Creating/writing to a file
```python
# Using with will automatically close the file
with f as open('test.txt', 'w+'):
    f.write("Testing 1,2,3...")
```

## Reading directories
```python
import os
print(os.getcwd()) # Returns working directory (where you run python command from)
print(os.listdir()) # Returns everything in current dir
print(os.listdir('../some_path')) 

# You can also 'walk' a directory to get a tuple of directory info for each entry
for folder,sub_folders,files in os.walk(os.getcwd() + "/src"):
    print(f"Currently looking at {folder}")
    print(f"Subfolders are {sub_folders}")
    print(f"Files in this dir are {files}")
```

## Moving files
```python
import shutil
shutil.move('test.txt', '../dest');
```

## Deleting files
```python
os.unlink('test.txt')
```