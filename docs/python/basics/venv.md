# Virtual Environments

Virtual environments are a way of creating a unique workspace for each of your python projects so that you can have different versions of dependencies installed on one machine.  Once in the virtual environment, any packages installed will only apply to that enviroment.

### To Install
```bash
pip install virtualenv
```

### To Create
```bash
virtualenv <env_name>
OR
python -m venv <env_name>
```

### To start
```bash
On Unix:
source <env_name>/bin/activate

On Windows:
<env_name>/Scripts/activate
```

### To exit
```bash
deactivate
```

### Requirements file
```bash
To save:
pip freeze > requirements.txt

To install:
pip install -r requirements.txt
```