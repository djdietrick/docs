# Classes

## Basics

Classes are defined by the `class` keyword and are followed by `():`.  The constructor is defined by the `__init__` function, where you can pass in parameters and save them on the class instance.  The destructor is defined by the `__del__` function.

You can also define class object variable outside of the constructor, and this will apply to all instances of the class.  This is mutable but then only applies to that instance of the class.  

You define class methods the same way as you do normal [functions](./functions).  Pass in the `self` keyword to access instance variables.

```python
class Foo():
    bar = 'bar' # Class object variable
    
    # Constructor
    def __init__(self, name):
        self.name = name # Instance variable

    # Destructor
    def __del__(self):
        print("{} died :(".format(self.name))

    def func(self):
        print(self.prop)
```

## Overriding methods

You can override default methods within your classes to perform different behavior based on the class instance.  This can be done with base python methods such as `__str__` or `__len__`, as well as any other inheritted functions.

```python
class Foo():
    # This would appear if you performed print(foo) where foo is an instance of Foo
    def __str__(self):
        return f"Hello, my name is {self.name}"

    def __len__(self):
        return 5
```

## Inheritance

To inherit from a class in python, you simply provide that class within the parenthesis when defining your child class.

To create abstract classes, python provides a `NotImplementedError` exception that will throw if the function is attempted to be called by a class that does not define it.

```python
class Animal():
    def __init__(self, name):
        self.name = name

    def who_am_i(self):
        print("I am an animal")
    
    def eat(self):
        print("I am eating")

    # Makes Animal an abstract class
    def speak(self):
        raise NotImplementedError("Subclass must implement this abstract method")

class Dog(Animal):

    # Class object attribute, the same for all instances
    species = 'mammal'

    def __init__(self, breed, name):
        Animal.__init__(self,name)
        self.breed = breed
        self.name = name

    def __str__(self):
        return self.name

    def bark(self):
        print("Woof! My name is {}".format(self.name))

    # Overridden function from base class
    def who_am_i(self):
        print("I am a dog!")

    # Implementing the abstact function
    def speak(self):
        return self.name + " says woof!"

class Cat(Animal):
    def __init__(self, name):
        Animal.__init__(self,name)
    
    def speak(self):
        return self.name + " says meow!"

my_dog = Dog(breed='Lab', name='Frankie')
print(my_dog.breed) # Lab
print(my_dog) # Frankie, because we overrode the __str__ operator
my_dog.eat() # Calling method on base Animal class
my_dog.who_am_i() # Calling instance on Dog class

my_cat = Cat('Sampson')

for pet in [my_dog, my_cat]:
    print(type(pet)) # Dog, Cat
    print(pet.speak()) # says woof!, says meow!
```