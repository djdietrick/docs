# Decorators

Decorators can be added to functions to run additional code before or after the function run. 

```python
def decorator(original_func):
    def wrap_func():
        print("Before executing original function")
        original_func()
        print("After executing original function")
    return wrap_func

@decorator
def my_func():
    print("Executing my function")
```