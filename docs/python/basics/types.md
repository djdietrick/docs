# Types

You can find out the type of an object in python with the `type` function.

```python
x = 5
print(type(x)) # int
```

| Type      | Variations    |
| --------- | ------------- |
| Text      | `str`         |
| Numeric   | `int`, `float`, `complex` |
| Sequence  | `list`, `tuple`, `range` |
| Mapping   | `dict`        |
| Set       | `set`, `frozenset` |
| Boolean   | `bool`        |
| Binary    | `bytes`, `bytearray`, `memoryview` |
| None      | `NoneType`    |