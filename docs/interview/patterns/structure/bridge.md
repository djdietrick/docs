# Bridge

A bridge is a mechanism that decouples an interface from an implementation.  This is a fancy way of saying passing an object into another object to have it perform some other logic that is not the responsibility of the containing object.  In the below example, a shape can be drawn in either vector or raster.  The Circle class should not contain the logic of how to draw that shape in a particular format, so it uses the renderer as a bridge to perform that duty.  This means that as you create more combinations of possibilities you limit the number of places you need to change.

```python
class Renderer():
    def render_circle(self, radius):
        pass

class VectorRenderer(Renderer):
    def render_circle(self, radius):
        print(f'Drawing a circle of radius {radius}')

class RasterRenderer(Renderer):
    def render_circle(self, radius):
        print(f'Drawing pixels for circle of radius {radius}')

class Shape:
    def __init__(self, renderer):
        self.renderer = renderer

    def draw(self): pass
    def resize(self, factor): pass

class Circle(Shape):
    def __init__(self, renderer, radius):
        super().__init__(renderer)
        self.radius = radius

    def draw(self):
        self.renderer.render_circle(self.radius)

    def resize(self, factor):
        self.radius *= factor

if __name__ == '__main__':
    raster = RasterRenderer()
    vector = VectorRenderer()
    circle = Circle(vector, 5)
    circle.draw()
    circle.resize(2)
    circle.draw()
```