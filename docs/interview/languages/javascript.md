# JavaScript

## Arrays

### Creating

```javascript
let a = [];
let a = [1, 2, 3];
let a = Array(3).fill(0); // [0,0,0]
let a = Array.from({ length: 5 }, (_, i) => i + 1); // [1,2,3,4,5]
let a = [...Array(5).keys()]; // [0,1,2,3,4]
```

### Accessing

```javascript
const a = [1, 2, 3];
console.log(a[0]); // 1
console.log(a.at(-1)); // 3 (last element)
```

### Manipulating

#### Adding
```javascript
const a = [];
a.push(1); // Add to end
a.unshift(2); // Add to beginning
a.splice(1, 0, 3); // Insert 3 at index 1
[...a, 4]; // Create new array with 4 added
```

#### Removing
```javascript
const arr = ['bread', 'butter', 'eggs', 'milk'];
arr.pop(); // returns and removes 'milk'
arr.shift(); // returns and removes 'bread'
arr.splice(1, 1); // removes 'butter'
```

#### Slicing
```javascript
const a = ['spam', 'egg', 'bacon', 'tomato', 'ham', 'lobster'];
a.slice(2, 5); // ['bacon', 'tomato', 'ham']
a.slice(-3); // ['tomato', 'ham', 'lobster']
a.slice(2); // ['bacon', 'tomato', 'ham', 'lobster']
[...a]; // Create a copy of an array
```

#### Sorting
```javascript
const nums = [3, 1, 3, 2, 5];
nums.sort(); // [1, 2, 3, 3, 5]
nums.reverse(); // [5, 3, 3, 2, 1]
[...nums].sort((a, b) => b - a); // Sort descending, creates new array

const items = [
    { name: 'john', grade: 'A', age: 15 },
    { name: 'jane', grade: 'B', age: 12 },
    { name: 'dave', grade: 'B', age: 10 }
];
items.sort((a, b) => a.age - b.age);
```

#### Filtering
```javascript
const odds = [1, 2, 3, 4, 5].filter(x => x % 2 === 1);
const evens = Array.from({ length: 20 }, (_, i) => i + 1).filter(x => x % 2 === 0);
```

## Strings

Strings in JavaScript are immutable but have many built-in methods.

### Helper Functions

```javascript
const str = 'spam';
'I saw spamalot!'.includes(str); // true
'I saw The Holy Grail!'.includes(str); // false

'12345'.repeat(5); // '1234512345123451234512345'

['John', 'Peter', 'Vicky'].join('#'); // 'John#Peter#Vicky'
'Hello, world!'.endsWith('!'); // true
'Hello, world!'.startsWith('H'); // true
'Hello, world!'.substring(0, 5); // 'Hello'
'Hello, world!'.slice(-6); // 'world!'
```

## Objects

### Creating

```javascript
const obj = {};
const obj = { color: 'green', points: 5 };
const obj = Object.create(null);
const obj = new Object();
```

### Accessing

```javascript
obj.color;
obj['color'];
obj?.points; // Optional chaining
const { color, points } = obj; // Destructuring

obj.age = 30;
Object.assign(obj, { height: 180 }); // Merge properties
```

### Removing

```javascript
delete obj.age;
```

### Iterating

```javascript
Object.keys(obj).forEach(key => {
    // do something
});

Object.values(obj).forEach(value => {
    // do something
});

Object.entries(obj).forEach(([key, value]) => {
    // do something
});
```

### Object Methods

```javascript
const keys = Object.keys(obj);
const values = Object.values(obj);
const entries = Object.entries(obj);
const merged = Object.assign({}, obj1, obj2);
const clone = { ...obj };

// Prevent modifications
Object.freeze(obj);
Object.seal(obj);
```

## Sets and Maps

### Set

```javascript
const set = new Set();
set.add(1);
set.has(1); // true
set.delete(1);
set.clear();

const unique = [...new Set([1, 2, 2, 3, 3])]; // [1, 2, 3]
```

### Map

```javascript
const map = new Map();
map.set('key', 'value');
map.get('key');
map.has('key');
map.delete('key');
map.clear();

// Object to Map
const map = new Map(Object.entries(obj));

// Map to Object
const obj = Object.fromEntries(map);
```

