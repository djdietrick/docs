# Memory

Memory is the mechanism by which a computer keeps data.  It's a collection of memory slots that can hold bits (0's and 1's).  Each memory slot can hold 8 bits, or 1 byte.  Our typical fixed-width integers we use in programming are 32-bit integers, meaning they take up 4 bytes of memory to represent a number.  These bytes need to be contiguous in order to store these integers.  Accessing these memory slots directly is very fast, similar to looking up a value in an array by it's index.

Endianness describes the order in which these bytes are stored in memory.  Big-endian means the most significant byte comes first in the sequence, and little-endian means the least significant bit comes first.

Pointers are values stored in memory that are addresses of other memory slots.  Each memory slot is identified by a memory address, which in itself is a base 2 integer.  This helps solve the problem of having to store values contiguously.  You can instead store pointers next to each other that represent that actual values stored elsewhere where there is space.