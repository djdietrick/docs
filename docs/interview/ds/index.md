# Data Structures

Data structures are just a way to organize and manage data. They provide a structure that allows you to perform algorithms to manipulate this data in different ways.  Each data structure has different relationships between its elements and have different strengths and weaknesses.

