# Scaling

Scaling is the problem of supporting more and more users as your applications grow.  These are ways to increase your throughput. In order to handle more requests, we need more hardware.

## Horizontal Scaling

Horizontal scaling increases capability by adding machines to handle requests from users. This allows the processing to be split up across multiple machines and the requests are routed to available machines.

Some of the advantages of this approach are resiliency and overall scalability.  It is resilient because if one node fails, then the load can automatically be distributed across the available nodes and the service is uninterrupted to the user.  It is better for overall scalability because as your usage increases, you can simply add more machines to the network.

Some of the disadvantages of this approad are requiring load balancing, slower interprocess communication, and data inconsistency.  Load balancing is required to direct requests to available nodes which is another level of abstraction in our system.  If processes running on different nodes need to communicate with one another, this is slower than a vertical solution because this requires network calls which will be slower than interprocess communication.  In addition, if these processes require the same data, it can be inconsistent at any given moment as it takes time to update all the dependent processes of any changes.

## Vertical Scaling

Vertical scaling is a method of increasing scalability by adding capablility to a single machine.  To process more requests, you make your server faster by increasing the hardware.  

Some of the advantages of this approach compared to horizontal scaling are that there is no load balancing required, communication between processes is faster, and data will be consistent among processes on the server.

Some of the disadvantages of this approach are reliability and hardware limitations.  As opposed to the horizontal solution, if the node fails then the service will be unavailable.  We have a single point of failure.  Scaling is also limited because there is a limit to how powerful you can make a machine.  Once that limit is reached you will need to scale horizontally as well.