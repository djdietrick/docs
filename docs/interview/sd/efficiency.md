# Measuring Efficiency

When describing the efficiency of a system, we look at a few different properties.

## Latency

Latency describes the time it takes for a machine to perform a certain operation.  Every method of retrieving information has different time costs.  A general rule of thumb:

- Reading 1MB from RAM - 0.25ms
- Reading 1MB from SSD - 1ms
- Transfer 1MB over network - 10ms
- Reading 1MB from HDD - 20ms
- Intercontinental round-trip - 150ms

## Throughput

While latency measures the time operations take, throughput describes the number of operations that can be processed in a given amount of time, such as requests per second.

## Availablility

Availability describes the percentage of time when a server is up and running.  This is usually measured in percentages or more typically **nines**.  If a server is available 99% of the time, we say that it has 2 nines of availability.  A system is **highly available** if it has five or more nines of availability.

### Redundancy

Redundency is a means of increasing availability by having copies of the system ready to go in case the main system fails.