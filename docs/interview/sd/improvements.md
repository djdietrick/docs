# Improving Efficiency

## Caching

Caching is the process of saving data so that it is faster to retrieve than doing all the way to the data source or performing expensive computations.  This can reduce the overall processing time at the cost of using extra memory.  An example of caching at a large scale are **CDNs**, or Content Delivery Networks, which are third party caches used to cache website data such as Cloudflare and Google Cloud CDN.  Some other popular caching softwares include:

- Redis: Really fast in memory key-value store with some persistent storage options, often used in rate limiting.
- Etcd: Strongly consistent and highly available key-value store, often used in leader election.
- Zookeeper: Another strongly consistent and highly available key-value store, often used for configuration or leader election.

## Proxies

Proxies are processes that stand between the client and server. **Forward** proxies work on the behalf of the client, such as a VPN.  **Reverse** proxies act on the behalf of the server, such as loggers, load balancers, and caches.

### Load balancers

Load balancers receive all traffic for a service and distribute it to multiple processes.  The use a **Server-Selection Strategy** to determine how to divide the traffic.  Some strategies include round robin, random selection, performance-based selection, or location/IP-based selection.  If the server workload becomes un-even, then we have a **hot spot** and the selection strategy may need tuning.

## Replication

Replication is a method of increasing redundency by actively duplicating data from one database server to another.  It can also be used to decrease latency by copying data to servers closer to the user.

## Sharding

Sharding, or data partitioning, is the process of splitting databases in pieces to increase the throughput of the system.  By splitting the data, queries will be able to be performed faster on any single database server.  Sharding can be done by client region, type of data being stored, or some hashing function of a column.  This will require a load balancer to send the requests to the correct database server.

## Peer to Peer Networks

P2P networks use a collection of machines (or peers) to divide a workload amongst themselves and reduce the overall processing time.  This is especially useful for file distribution.  Instead of downloading a file from a single server, the file is spread in chunks to peers across the network.  Then a new peer can request those pieces of data from the other peers, spreading the strain from one server to many.  This can be done without a centralized source of data and is called a **Gossip Protocol**.