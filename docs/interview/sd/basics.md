# Basics of the Internet

## Client-Server Model

The internet is based on the client-server model.  Servers are processes that provides services to clients, and clients are processes which request things from the servers.  These machines find each other using their **IP addresses**, which are 4 numbers from 0 to 255 in a.b.c.d format, and communicate through **ports**.  A **DNS**, or Domain Name Server, links domain names to IP addresses.

::: details
Notable IP Addresses include 127.0.0.1 or localhost which is your own machine and 
192.168.c.d are machines on your local network.  Notable ports include 22 (ssh), 443 (https), 
80 (http), and 53 (DNS lookup).
:::

## Protocols

The above method of communication is known as Internet Protocol.  There are additional protocols built on top of IP.

### TCP

TCP allows for ordered, reliable data delivery between machines through connections.  This also exposes **sockets** which allow applications to stream data through an open connection.

### HTTP

HTTP (HyperText Transfer Protocol) is implemented on top of TCP.  This is the request-response model.  Requests typically have a method, headers, and a body as well as origin information, and the request typically has a status and some kind of data.