# Infrastructure

## CloudFormation

CloudFormation is a declaritive way of outlining your AWS services for almost any resource.  You can also define infrastructure programmatically via the CDK.

## Elastic Beanstalk

This gives you one view where you can manage most of your AWS services for an application (EC2, ASG, ELB, RDS, etc).  This view still gives you full control over the configuration of these services.  Beanstalk is free but you pay for the underlying resources.  That way you are only responsible for the application code.  Supports many languages and single/multi-container docker workloads.  There are three architecture models:

- Single instance - dev
- LB + ASG - Prod web apps
- ASG only - prod workers

## CodeDeploy

CodeDeploy is a method of deploying our applications automatically, working on both EC2 instances and on-prem servers.  There is a CodeDeploy agent that must be deployed on the receiving machines first.

## CodeCommit

Github for AWS.

## CodeBuild

Compiles source code, runs tests, and produces packages ready for deployment.

## CodePipeline

Orchestrate the steps your code goes through with the above services.  Go from commit to build to deploy.

## CodeArtifact

Artifact storage in AWS for dependencies and packages.  Works for most major dependency management tools.

## CodeStar

One stop shop for the above services.

## Cloud9

Cloud IDE for writing, running, and debugging code.  Allows for real-time pair programming.

## AWS Systems Manager

Central place to manage both EC2 instances and on-prem servers at scale.  Install an agent on the machines so they can communicate back.  This agent can run commands, patch, and configure our servers.

### SSM Session Manager

Allows you to start a secure shell on machines with the SSM Agent installed without SSH.  This is better for security by not needing to allow traffic on that port.  

### Parameter Store

Place to store keys, passwords, configs, etc.