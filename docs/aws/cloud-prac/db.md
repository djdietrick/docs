# Databases

AWS has many different types of database offerings.

## RDS

Relational Database Service offers managed DB services for different SQL flavors.  This means they have automated provisioning and patching, continuous backups, multi-AZ setup for disaster recovery, and more.  You can create read replicas of your DB in multiple AZs for faster read times around the world.  There are also multi-AZ deployments for failover, where you read/write to the main DB until it fails.

## Amazon Aurora

Cloud-optimized Postgres or MySQL database that has much better performance over normal RDS.  Storage automatically grows based on usage.  Costs more than RDS but is more efficient.  There is also a serverless offering that auto-scales based on load and you pay per second.  Good for infrequent or unpredictable workloads.

## ElastiCache

Managed Redis or Memcached service with in-memory databases for high performance and low latency.

## DynamoDB

Fully managed NoSQL database from AWS.  Uses a serverless model for scaling and has a very low latency retrieval.  Data is defined with a partition key and sort key which make up the primary key.  **DAX**, or DynamoDB Accelerator, is an in-memory cache for DynamoDB with significant performance improvements.  **Global Tables** can be used to replicate data to multiple regions and allow multi-region reads and writes.

## Redshift

Based on Postgres, but used for data processing, or OLAP (online analytical processing).  Think analytics and data warehousing.  You load data once an hour and it scales better for many PBs of data.  This also has a serverless offering for less frequent workloads.

## EMR

Elastic MapReduce is used for creating Hadoop clusters for Big Data.

## Athena

Serverless services to query data in S3 with SQL.  Supports CSV, JSON, and other file formats.  You pay $5 per TB scanned.

## QuickSight

ML powered dashboard for visualizing your data.

## DocumentDB

AWS implementation of MongoDB.  Fully managed with replication across 3 AZ.

## Neptune

Fully managed graph database.

## Timestream

Fully managed time series databse.

## QLDB

Quantum Ledger Database is a ledger type database for recording financial transactions.  It is immutable like the blockchain, but it is hosted by AWS.

## Amazon Managed Blockchain

This helps you build applications on the blockchain and make it decentralized.  It supports Hyperledger Fabric and Ethereum.

## AWS Glue

Managed ETL (extract, transform, load) service to prepare data for analytics.  Can be used to load data into Athena, Redshift, or EMR.

## Database Migration Service

Helps you transfer data from one database (on-prem) to another in AWS.  It allows the source database to operate while the migration is taking place.