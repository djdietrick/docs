# Load Balancing

Load balancing allows our application to be scalable and highly available.  Scalability is the ability to accommodate larger loads by increasing hardware or nodes.  Elasticity defines how the system auto-scales to meet demand.

## Elastic Load Balancer

ELB is a managed load balancer with four different flavors offered:

- Application LB - HTTP/HTTPS/gRPC (layer 7), HTTP routing, and static DNS
- Network LB - TCP/UDP (layer 4), high performance, static IP through Elastic IP
- Gateway LB - IP (layer 3), route traffic to firewall on EC2, intrusion detection

## Auto Scaling Group

ASGs allow you to spin up new compute instances to meet demand.  You define a minimum, desired, and maximum size and it will scale up or down depending on load.  It can also use predictive scaling to predict demand based on past trends.