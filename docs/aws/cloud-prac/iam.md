# Identity and Access Management

A global service for managing users, groups, and access to AWS resources. Users and groups can be assigned **policies** to define what they are allowed to do, or their permissions. You should follow the **least privilege principle** and don't give users more permissions than they need.

## Policy Structure

- Sid - Unique identifier
- Effect - Allow or Deny
- Principal - What the policy is applied to (account, user, group)
- Action - List of actions to allow or deny
- Resource - Actual resource these actions will be performed against
- Condition (optional) - When this policy is in effect

## Roles

Roles are used to assign permissions to AWS services or users.  They can be assigned and unassigned dynamically to temporarily allow privileges to perform an action.

## Security Tools

- IAM Credentials Report - List all your account's users and status of their credentials
- IAM Access Advisor - Shows permissions granted to a user and when they were last accessed