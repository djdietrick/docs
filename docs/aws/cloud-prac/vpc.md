# VPC

The Virtual Private Cloud is a private network in which all of your resources are deployed.  These can be partitioned into **Subnets** which can be either private or public.  Access to subnets is defined in **Route Tables**.

## Elastic IP

Attaches a fixed public IPv4 address to an EC2 instance.  Will cost you if not attached to an EC2 instance or it is stopped.

## Internet Gateway

Allows out public subnets to have access to the internet.

## NAT Gateways

AWS managed instances in your public subnets that allow instances in the private subnets to access the internet while remaining private.

## NACL

Network ACL (Access Control List) is a firewall which controls traffic to and from a subnet.  Can have allow and deny rules and is attached at the subnet level.  Rules can only include IP addresses, and return traffic must be explicitly allowed.

## Security Groups

Similar to NACL, but is attached to an EC2 instance.  Can only have allow rules, but rules can include IP addresses or other security groups.

## VPC Flow logs

Captures information about traffic through VPC, subnets, and Network interfaces.  Also captures neetwork traffic from other services (ELB, ElastiCache, RTS, etc).  Can go to S3, CW Logs, Kinesis.

## VPC Peering

Connects two VPCs privately through the AWS network.

## VPC Endpoints

Connect to AWS Services using a private network instead of public internet.  Gateway is for S3 and DynamoDB, Interface is for the rest.

## AWS PrivateLink

Exposes a service to 1000s of VPCs without peering.  Requires a Network Load Balancer and Elastic Network Interface.

## Site to Site VPN

Connect on-prem to AWS through VPN with connection encrypted, goes over public internet.

## Direct Connect (DX)

Phyisically install direct private connection to AWS.

## Transit Gateway

Connects thousands of VPCs and on-premises networks together from central location.