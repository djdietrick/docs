# Messaging

## SQS

Simple Queue Service allows producers to send message to a queue and listeners to consume those messages.  Oldest AWS offering.  SQS is a FIFO queue, so the messages are processed in order they are sent.

## Kinesis

Real-time big data streaming.

## SNS

SNS allows you to send messages to a topic which can then have multiple receivers that perform different actions based off that. 

## Amazon MQ

Fully managed message broker service for RabbitMQ and ActiveMQ.  Used for migrating to the cloud to work with existing on-prem infrastructure.  Doesn't scale as much as SQS and SNS.

