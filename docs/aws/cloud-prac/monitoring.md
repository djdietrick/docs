# Monitoring

## Cloudwatch

Provides metrics for every service in AWS.  From these metrics you can create dashboards.

### CloudWatch Alarms

Triggers notifications for any metric, which can then do auto-scaling, EC2 actions (stop, reboot, etc), or send SNS notifications.

### CloudWatch Logs

Collects logs from all your applications (Beanstalk, ECS, Lambda, Route 53, log agents on-prem).  For EC2, you need to run the agent as well to push the log files you want.

## EventBridge

This service allows you to trigger events on your system based on different parameters.  You can run cron jobs at certain times.  You can react to events happening, like users signing in, etc which can trigger lambda functions, send SQS/SNS messages, etc.  You can archive the events sent on an event bus and replay them.

## CloudTrail

Provides governance, compliance, and audit to your AWS account.  You can get a history of all API calls made from an AWS account.  Logs can go into CloudWatch logs or S3.

## X-Ray

Helps with debugging code in production breaking down metrics of our applications. It allows us to identify bottlenecks, understand dependencies, and other diagnostics.

## CodeGuru

ML-powered code review (Reviewer) and application performance recommendations (Profiler).

## AWS Health Dashboard

Shows health of services in all regions.

## AWS Account Health Dashboard

Shows health of services that you use and alerts you when things go down.  Global service.
