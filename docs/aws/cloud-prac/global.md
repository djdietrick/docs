# Global Infrastructure

## Route 53

Managed DNS that can route traffic to EC2 instances.  It can have different routing policies to route to different instances based on load, latency, or failover.

## CloudFront

CloudFront is a CDN, or Content Delivery Network, which caches data at the edge for faster read performance.  It also helps with DDoS protection because of integration with Shield and AWS WebApplication Firewall.

## S3 Transfer Acceleration

Increases the transfer speed by transferring file to AWS edge location and then using AWS's fast private network to put in S3 bucket.

## AWS Global Accelerator

Same concept, but for routing traffic to your load balancers and Route 53.

## AWS Outposts

On-prem servers that you can rent that run AWS services on them.

## AWS Wavelength

Brings AWS services to the edge of 5G networks.

## AWS Local Zones

Places where AWS has services in smaller cities, think extensions of an AWS region.  Good for latency-sensitive applications.

