# Lambda

Lambda functions are virtual functions that run on demand with no need to manage servers. Scaling is automated based on how many calls to the function are occuring.  These functions should be short execution times so they can run independently and be event-driven.  With Lambda you only pay for the number of requests and the compute time.  Lambda functions have a time limit of 15 mins, supports limited runtime languages, temporary disk space, and are serverless.

## API Gateway

Fully managed services for proxying requests to go to Lambda functions.  Serverless and scalable.  Supports REST and Websockets, and supports other API features like authentication, throttling, API keys,and monitoring.

## AWS Batch

Fully managed batch processing, running 100,000s of computing jobs on EC2 instances or Spot instances, but are defined as Docker images and run on ECS.  Batch jobs are different from lambda in that they have no time limit, can run in any language (docker), has persistent storage through EBS and instance stores, and rely on underlying EC2 instances that are managed by AWS.