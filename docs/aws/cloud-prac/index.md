# Cloud Practitioner

This section contains my notes for the Cloud Practitioner Foundational cirruculum.

## Cloud Basics

Cloud computing is the on-demand delivery of compute power, storage, applications, and other IT resources.

### Server Components

- Compute: CPU
- Memory: Ram
- Storage: Files and Database
- Networking: DNS, Firewall, Router, Switch, etc.

### Characteristics of CC

- On-demand self service
- Broad network access from outside sources
- Multi-tenancy and resource pooling, users share resources
- Rapid elasticity and scalability
- Measured service and cost

### Advantages

- Trade capital for operational support, don't need to maintain datacenteres
- Massive economies of scale with many users
- Capacity is scalable on the fly
- Increase efficiency
- Cost effective

### Types

- Infrastructure as a Service (Iaas)
    - Individual building blocks for your application like EC2
- Platform as a Service (Paas)
    - Managed infrastructure like Elastic Beanstalk
- Software as a Service
    - Completed product run and managed by provider like Rekognition for ML

### Pricing

- Compute - Pay for CPU time
- Storage - Pay for amount of data stored
- Data transfer - Only pay for transfer out of the cloud, transfer in is free!

### AWS Global Infrasturure

- Regions: Cluster of data centers located around the world, projects are scoped by region
- Availability Zones: Individual (or more) datacenters within a region
- Edge Locations: Delivers content to end users with low latency

### Shared Responsibility Model

AWS operates under an understanding of shared responsibiliy, where Amazon is responsible for the security of the cloud itself, while the customer is responsible for the security within the cloud, such as networking rules, access management, customer data, client/server side encryption, etc.