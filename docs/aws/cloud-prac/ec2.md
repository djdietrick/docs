# EC2

EC2 stands for Elastic Compute Cloud, and it is for creating virtual machines in the cloud.  These can be for running processes, storing data (EBS), distributing load (ELB), and scaling services (ASG). EC2 instances come in different sizes that you can pick for your use cases.  They can be optimized for computing, memory, or storage.

## AMI

AMIs, or Amazon Machine Images, are like docker images for AWS.  It defines the customization and configuration of an EC2 instance.  This allows you to launch pre-configured instances of your applications quickly.  AMIs are locked to a region but can be copied across.  **EC2 Image Builder** can be used to create and test AMIs and can be run on a schedule.

## Security Groups

Security Groups allow traffic into or out of our EC2 instances.  They can only contain allow rules and act as a firewall.  These groups can be attached to multiple instances as long as they are in the same region and VPC.  All inbound traffic is blocked by default, and all outbound traffic is allowed by default.

When debugging EC2 instances, if you are getting timeouts it is most likely a SG issue.  If the connection is refused, then it is an issue with the application.

## Purchasing Options

- On-Demand - Short workload, predictable pricing, pay by second
- Reserved - Reserve for 1 or 3 years for a discount
- Savings Plans - Commit to amount of usage, get discount. Doesn't need to be utilized 24/7
- Spot instances - Bid on unused EC2 instances, can be lost if you are outbid
- Dedicated hosts - Reserve and entire physical server, choose location
- Dedicated instances - No other customer will share your hardware, can't choose location
- Capacity Reservations - Reserve capacity in specific AZ for any duration

## Storage

### EBS

EBS, or Elastic Block Storage, is a network drive that you can attach to an EC2 instance.  Data will persist after termination of the instance.  EBS volumes can only be mounted to one instance at a time and are bound to an AZ.  One EC2 instance can have multiple EBS volumes attached.  You can also take a Snapshot to backup your volume which you can copy across AZ and region.

### EC2 Instance Store

These are similar to EBS but instead of network drives, these are hardware disks so they will have better performance.  However, they are ephemeral so they are good for caches.

### EFS

EFS, or Elastic File System, works as a normal mounted NAS.  If can be mounted on multiple EC2 instances across multiple AZs (if linux).  This is more expensive but highly available and scalable.  There is also an **Infrequent Access** tier that is lower cost if you access the data less.

### Amazon FSx

- For Windows File Server - Windows native shared file system, fully managed and highly available.
- For Lustre - File storage for high performance computing

## Docker

AWS also has some services that host docker containers similar to EC2 instances.

### ECR

Elastic Container Registry is AWS's container storing solution.  This is private in your AWS account.

### ECS

Elastic Container Service launches dowcker containers on AWS via EC2 instances which you need to provision yourself.  Can be integrated with an Application Load Balancer to scale the containers.

### Fargate

Fargate is a serverless way to run your docker containers on demand.  There is no need to provision your own EC2 instances.

## Lightsail

Lightsail is a service for people with little cloud experience to launch templates that under the hood produce EC2 instances, RDS, ELB, etc for web applications.  It does not support auto-scaling and has a limited number of services available.