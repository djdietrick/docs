# Directory Structure

## Pages

Contains all of our vue files that make up our main page layout.  The directory structure also determines the routes (see [routing](/nuxt/routing)).

## Components

Contains all of the vue components that you want to use in multiple places in your application.  These components get auto imported into all of your other files and are treeshaken to remove unused ones.  The component name is dependent on the file location, so `components/base/foo/Button.vue` will be `BaseFooButton`.  Then can also be chosen dynamically like below.

```vue
<template>
  <component :is="clickable ? MyButton : 'div'" />
</template>

<script setup>
const MyButton = resolveComponent('MyButton')
</script>
```

## Composables

Contains javascript files that are auto imported, although only files at the `composable` root level and index files in subdirectories and only exported named functions are available.  This is also where you would define your [state](/nuxt/state).

## Server

See [Server Routes](/nuxt/server)

## Assets

Contains styles, images, and fonts.

## Static

This is directory mapped to the server root and contains files that have to keep their names (like images, etc) or likely won't change (favicon).