# Routing

Routing in Nuxt is file based. The file structure within the `pages` directory determines the routes for the front end application. 

Files with `index.vue` within a directory will route to that directories name, while files with other names will append that file name onto the route. 

Dynamic routes are denoted by square brackets surrounding the file name.  These can be accessed with `this.$route.params.{parameterName}`.  You can also have a catchall route with`[...slug].vue`. This will hit if the url matches no other routes.  If multiple unknown routes are provided, they will come in as an array `/hello/world -> ['hello', 'world']`.  You can also denote optional parameters by wrapping it in two sets of brackets.

- `pages/index.vue` -> /
- `pages/about/index.vue` -> /about
- `pages/about/me.vue` -> /about/me
- `pages/profile/[id].vue` -> /profile/foo
- `pages/users-[group]/[id].vue` -> /users-foo/bar
- `pages/[...slug].vue` -> /foobar (catchall)

## Accessing Route in Vue files

```vue
<script setup>
const route = useRoute()

if (route.params.group === 'admins' && !route.params.id) {
  console.log('Warning! Make sure user is authenticated!')
}
</script>

<template>
  <p>{{ $route.params.slug }}</p>
</template>
```

## Navigation

```vue
<template>
  <NuxtLink to="/">Home page</NuxtLink>
  <NuxtLink to="/about">About page</NuxtLink>
  <NuxtLink to=`/user/${id}`>User page</NuxtLink>
  <a href="https://nuxtjs.org">External Link to another page</a>
</template>
```