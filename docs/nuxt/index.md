# Nuxt.js

Nuxt is a server side rendering framework for Node and Vue.

## Creating a Project

```bash
npx nuxi init <project-name>
cd <project-name>
npm run dev
```