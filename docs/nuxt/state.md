# State Management

State is created using the `useState` function. Any component that calls `useState` with the same key will share the state for that variable.

```jsx
const counter = useState('counter', () => Math.round(Math.random() * 1000))

function increment() {
	counter++;
}
```

You can also create shared state by creating a composable that returns state functions. If using `useState` outside of a setup function, they must be created this way otherwise they will be shared by every user of the application, not just the current one.

```tsx
// composables/states.ts
export const useCounter = () => useState<number>('counter', () => 0)
export const useColor = () => useState<string>('color', () => 'pink')
```