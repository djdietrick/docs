# Types

## Scalar Types

```rust
// Integer, either signed or unsigned, bits in (8,16,32,64,128)
let _num: i32 = -5; 
let _unsigned_num: u32 = 5;

// Floats, either 32 or 64 bit
let _f = 5.0; // Defaults to f64
let _f2: f32 = 3.9;

// Boolean
let _b: bool = true;

// Char, specified by single-quotes
let _c: char = 'c';
```

## Slices

A slice is a reference to a sequence of elements in a collection.  Since it is a reference, it does not have ownership.

```rust
// Slice
let s = String::from("hello world");

// String slice &str
let _hello: &str = &s[0..5];
let _world = &s[6..11];
let _world2 = &s[6..];
let _whole = &s[..];

// Works with both &str and String
fn first_word(s: &str) -> &str {
    s
}

// Can also slice arrays
let a = [1, 2, 3, 4, 5];

let _slice = &a[1..3];
```

## Tuples

Tuples have a fixed length, and are immutable.

```rust
// Tuples have a fixed length, once declared you can't change
let tup: (i32, f64, u8) = (500, 6.4, 1);

// Destructuring
let (_x,_y,_z) = tup;

//Accessing
let _five_hundred = tup.0;
let _six_point_four = tup.1;


```

## Arrays

Arrays in Rust have a fixed length. They allocate on teh stack instead of on the heap.

```rust
let a = [1, 2, 3, 4, 5];

// Specify type and length
let b: [i32; 5] = [1, 2, 3, 4, 5];

// Create with initial value
let _c = [3; 5]; // [3,3,3,3,3]

// Accessing
let _one = a[0];
let _two = b[1];

// Won't compile because we know the size at compile time
// let n = a[10];
```

## Result

Results allow you to handle when something returns either a value or error.  If it has a valid value, it will be of type `Ok<T>`, otherwise it will be of type `Err<E>`. See the `errors` section for more into on Results.

## Option

Options are similar to Results in that they can contain a conditional type.  It can be either of type `None` or type `Some<T>`.

```rust
enum Option<T> {
    None,
    Some(T),
}
```

## Type Aliases

```rust
type Kilometers = i32;
type Thunk = Box<dyn Fn() + Send + 'static>;

// RESULT SHORTHAND
type Result<T> = std::result::Result<T, std::io::Error>;
fn _result_shorthand() -> Result<()> {
    Ok(())
}
```