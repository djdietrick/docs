# Enums

```rust
// Simple enum
enum Difficulty {
    Easy,
    Medium,
    Hard
}
let _diff = Difficulty::Easy;

// Store values in enums
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}
let _mess = Message::Write(String::from("message"));
```

## Built in Enums

### Result

```rust
enum Result<T,E> {
     Ok(T),
     Err(E)
}
```

### Option

```rust
enum Option<T> {
     Some(T),
     None    
}

let _absent_option: Option<i32> = None;
let _present_option: Option<i32> = Some(5);
```