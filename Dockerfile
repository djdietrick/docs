FROM node AS build
WORKDIR /build
COPY . .
RUN npm install && npm run build

EXPOSE 8080

ENTRYPOINT [ "npm", "run", "serve"]
